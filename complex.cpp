#include "complex.h"

// put the definitions here

Complex & Complex::operator*(const Complex & other)
{
	Complex result;
	result._r = _r * other._r - _i * other._i;
	result._i = _r * other._i + _i * other._r;
	return result;
}

Complex & Complex::operator/(const Complex & other)
{
	Complex result;
	result._r = (_r*other._r + _i * other._i) / (other._r*other._r + other._i*other._i);
	result._i = _i * other._r - _r * other._i;
	result._i/= (other._r*other._r + other._i*other._i);
	return result;
}

Complex & Complex::operator+=(const Complex & other)
{
	this->_r = _r + other._r;
	this->_i = _i + other._i;
	return *this;
}

Complex & Complex::operator-=(const Complex & other)
{
	this->_r = _r - other._r;
	this->_i = _i - other._i;
	return *this;
}

Complex & Complex::operator*=(const Complex & other)
{
	
	this->_r = _r * other._r;
	this->_i = _i * other._i;
	return *this;
}

Complex & Complex::operator/=(const Complex & other)
{
	this->_r = _r / other._r;
	this->_i = _i / other._i;
	return *this;
}

Complex::Complex() :
	_r(0),
	_i(0)
{
}

Complex::Complex(const int& real, const int& imaginary) :
	_r(real),
	_i(imaginary)
{
}

Complex::Complex(const int& real) :
	_r(real),
	_i(0)
{


}

void Complex::accumulate(Complex & complexNumber)
{
	_r += complexNumber._r;
	_i += complexNumber._i;

	//return *this;
}

double Complex::Real() const
{
	return _r;
}

double Complex::Imaginary() const
{
	return _i;
}

void Complex::accumulate(const double & real)
{
	_r += real;
	_i += 0.0;
}

void Complex::accumulate(const double& real, const double& imaginary)
{
	_r += real;
	_i += imaginary;
}

double fabs(const Complex & obj)
{
	double res = sqrt(obj._r*obj._r + obj._i*obj._i);
	return res;
}

void printComplex(const Complex & complexNumber)
{
	std::cout << complexNumber._r << " + " << complexNumber._i << "i" << std::endl;
}

void printComplex(std::ostream& flux,const Complex& complexNumber)
{
	flux << complexNumber._r << " + " << complexNumber._i << "i";
}

std::ostream& operator<< (std::ostream& fout, const Complex& number)
{
	fout << number._r << " + " << number._i << "i ";

	return fout;
}

std::istream& operator>>(std::istream& fin, Complex& number)
{
	fin >> number._r >> number._i;

	return fin;
}

Complex Complex::operator + (const Complex& other)
{
	Complex temporaryNumber;

	temporaryNumber._r = _r + other._r;
	temporaryNumber._i = _i + other._i;

	return temporaryNumber;

}

Complex Complex::operator - (const Complex& other)
{
	Complex temporaryNumber;

	temporaryNumber._r = _r - other._r;
	temporaryNumber._i = _i - other._i;

	return temporaryNumber;
}