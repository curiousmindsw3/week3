#pragma once
class Point
{
private:
	double m_x, m_y;
public:
	Point();
	Point(const double& a, const double& b);
	 
	friend double distance(Point a, Point b);

	void SetX(double& value)
	{
		m_x = value;
	}

	void SetY(double& value)
	{
		m_y = value;
	}

	double GetX() const
	{
		return m_x;
	}

	double GetY() const
	{
		return m_y;
	}

	~Point();
};

