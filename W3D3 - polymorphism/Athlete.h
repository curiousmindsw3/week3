#pragma once
#include <string>
#include <iostream>

class Athlete
{
protected:
	std::string m_name;
	std::string m_nationality;

public:
	Athlete();

	Athlete(const std::string& name, const std::string& nationality);

	~Athlete();

	virtual void Interrogate();

	virtual std::string GetSport() const = 0;
};

