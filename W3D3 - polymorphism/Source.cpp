#include <iostream>
#include "Athlete.h"
#include "Runner.h"
#include "Swimmer.h"
#include "TennisPlayer.h"
#include <vector>
#include <typeinfo>

int main()
{
	std::vector<Athlete*> athlets;

	athlets.push_back(new TennisPlayer("Joe", "Canada", "tennis player"));
	athlets.push_back(new TennisPlayer("Halep", "Romania", "tennis player"));
	athlets.push_back(new Swimmer("Mark", "SUA", "swimmer"));
	athlets.push_back(new Runner("Tommas", "France", "runner"));

	for (auto athlete : athlets)
	{
		athlete->Interrogate();

		std::cout << typeid(*athlete).name() << ": ";
	}

	for (auto athlete : athlets)
	{
		delete athlete;
	}

	return 0;
}