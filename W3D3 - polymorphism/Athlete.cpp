#include "Athlete.h"

Athlete::Athlete() :
	m_name("Unknown athlete"),
	m_nationality("Unknown nationality")
{
}

Athlete::Athlete(const std::string & name, const std::string & nationality):
	m_name(name),
	m_nationality(nationality)
{
}

Athlete::~Athlete()
{
}

void Athlete::Interrogate()
{
	std::cout << "My name is: " << m_name << ", I am from " << m_nationality << " and I am a "<<GetSport()<<std::endl;
}
