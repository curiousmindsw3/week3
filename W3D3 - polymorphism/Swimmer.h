#pragma once
#include "Athlete.h"

class Swimmer : public Athlete
{
protected:
	std::string m_sport;

public:
	Swimmer();

	Swimmer(const std::string& name, const std::string& nationality, const std::string& sport);

	~Swimmer();

	std::string GetSport() const override;
};

