#include "Runner.h"

Runner::Runner()
{
}

Runner::Runner(const std::string & name, const std::string & nationality, const std::string & sport):
	Athlete(name, nationality),
	m_sport(sport)
{
}

Runner::~Runner()
{
}

std::string Runner::GetSport() const
{
	return m_sport;
}
