#pragma once
#include "Athlete.h"

class TennisPlayer : public Athlete
{
protected:
	std::string m_sport;

public:
	TennisPlayer();

	TennisPlayer(const std::string& name, const std::string& nationality, const std::string& sport);

	~TennisPlayer();

	std::string GetSport() const override;
};

