#include "TennisPlayer.h"

TennisPlayer::TennisPlayer()
{
}

TennisPlayer::TennisPlayer(const std::string & name, const std::string & nationality, const std::string & sport):
	Athlete(name, nationality),
	m_sport(sport)
{
}

TennisPlayer::~TennisPlayer()
{
}

std::string TennisPlayer::GetSport() const
{
	return m_sport;
}
