#include "Swimmer.h"

Swimmer::Swimmer()
{
}

Swimmer::Swimmer(const std::string & name, const std::string & nationality, const std::string & sport) :
	Athlete(name, nationality),
	m_sport(sport)
{
}

Swimmer::~Swimmer()
{
}

std::string Swimmer::GetSport() const
{
	return m_sport;
}
