#pragma once
#include "Athlete.h"

class Runner : public Athlete
{
protected:
	std::string m_sport;

public:
	Runner();

	Runner(const std::string& name, const std::string& nationality, const std::string& sport);

	~Runner();

	std::string GetSport() const override;
};

