#pragma once
#include <iostream>
#include <math.h>
//fabs, *,/

// put your declarations here
class Complex {
    double _r, _i;
public:
	//------Ionut------------------
	friend double fabs(const Complex& obj);
	Complex & operator* (const Complex& other);
	Complex& operator /(const Complex& other);
	//-----------------------------

	//--------Claudiu--------------
	Complex & operator+= (const Complex& other);
	Complex & operator-= (const Complex& other);
	Complex & operator*= (const Complex& other);
	Complex & operator/= (const Complex& other);
	//-----------------------------
	Complex();

	Complex(const int& real, const int& imaginary);

	Complex(const int& real);

	void accumulate(Complex& complexNumber);

	void accumulate(const double& real);

	void accumulate(const double& real, const double& imaginary);

	friend void printComplex(const Complex& complexNumber);

	friend void printComplex(std::ostream& flux,const  Complex& complexNumber);

	friend std::ostream& operator<< (std::ostream& fout, const Complex& number);

	friend std::istream& operator>>(std::istream& fin, Complex& number);

	Complex operator + (const Complex& other);

	Complex operator - (const Complex& other);

    double Real() const ;
    double Imaginary() const;
};
