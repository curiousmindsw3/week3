#include "Point.h"
#include <algorithm>



Point::Point()
{
}

double distance(Point a, Point b)
{
	return sqrt((b.m_x - a.m_x)*(b.m_x - a.m_x) - (b.m_y - a.m_y)*(b.m_y - a.m_y));
}


Point::Point(const double & a, const double & b):
	m_x(a), m_y(b)
{
}

Point::~Point()
{
}
