#include "ITriangle.h"



ITriangle::ITriangle()
{
}

ITriangle::ITriangle(const float & l1, const float & l2):
	ETriangle(l1), m_length1(l2)
{

}


ITriangle::~ITriangle()
{
}

float ITriangle::Perimeter()const
{
	return 2 * m_length + m_length1;
}
