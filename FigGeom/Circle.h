#pragma once
#include "GeometricShape.h"

class Circle : public GeometricShape
{
protected:
	float m_radius;

public:
	Circle();

	Circle(const float& radius);

	float Perimeter() const  override;

	~Circle();

};

