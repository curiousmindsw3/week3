#pragma once
#include "Square.h"
class Rhombus : public Square
{
public:
	float Perimeter()const  override;
	Rhombus();
	Rhombus(const float& l);
	~Rhombus();
};

