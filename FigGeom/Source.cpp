#include "ETriangle.h"
#include "Square.h"
#include "Rhombus.h"
#include "Rectangle.h"
#include "Triangle.h"
#include "ITriangle.h"
#include "Trapeze.h"
#include "ITrapeze.h"
#include "Circle.h"
#include "Hexagon.h"
#include "Cube.h"
#include <algorithm>
#include <vector>
#include <iostream>
int main()
{
	Triangle t(4, 8, 2);
	Square p(5);
	Rhombus r(3);
	Rectangle d(5, 1);
	ITriangle isosceles(3, 4);
	Trapeze tr(3, 5, 6, 2);
	ITrapeze trisosceles(5, 6, 2);
	Circle c(3);
	Hexagon h(3.2);
	Cube cube(2);
	Cube secondCube(p);

	std::vector< GeometricShape*> vector;
	vector.push_back(&t);
	vector.push_back(&p);
	vector.push_back(&r);
	vector.push_back(&d);
	vector.push_back(&isosceles);
	vector.push_back(&tr);
	vector.push_back(&trisosceles);
	vector.push_back(&c);
	vector.push_back(&h);

	std::sort(vector.begin(), vector.end(), [](GeometricShape* firstShape, GeometricShape* SECONDShape) { return firstShape->Perimeter() < SECONDShape->Perimeter();});
	//std::sort(vector.begin(), vector.end());
	for (auto v : vector) {
		std::cout <<"Perimeter is: " <<v->Perimeter() << std::endl;
	}

    return 0;
}

