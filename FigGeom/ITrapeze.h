#pragma once
#include "GeometricShape.h"

class ITrapeze : public GeometricShape
{
protected:
	float m_length1, m_length2, m_length;

public:
	ITrapeze();

	ITrapeze(const float& l1, const float& l2, const float& l3);

	float Perimeter() const override;

	~ITrapeze();
};

