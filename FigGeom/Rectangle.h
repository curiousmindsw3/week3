#pragma once
#include "Square.h"
class Rectangle : public Square
{
protected: 
	float m_height;
public:
	float Perimeter()const override;
	Rectangle();
	Rectangle(const float& l, const float& h);
	~Rectangle();
};

