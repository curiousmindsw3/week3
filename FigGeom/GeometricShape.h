#pragma once
#include<iostream>
class GeometricShape
{
public:
	GeometricShape();
	~GeometricShape();
	bool operator < ( GeometricShape* other) const;
	virtual float Perimeter()const =0 ;
};

