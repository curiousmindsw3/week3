#include "Circle.h"



Circle::Circle()
{
}

Circle::Circle(const float& radius):
	m_radius(radius)
{
}

float Circle::Perimeter() const
{
	return 2* 3.14f* m_radius ;
}


Circle::~Circle()
{
}
