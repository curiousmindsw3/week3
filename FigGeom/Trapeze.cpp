#include "Trapeze.h"



Trapeze::Trapeze()
{
}

Trapeze::Trapeze(const float & l1, const float & l2, const float & l3, const float & l4) :
	ITrapeze(l1, l2, l4),
	m_length3(l3)
{
}


Trapeze::~Trapeze()
{
}

float Trapeze::Perimeter() const
{
	return m_length1 + m_length2 + m_length3 + m_length;
}
