#pragma once
#include "Square.h"
class Hexagon : public Square
{
public:
	Hexagon();

	Hexagon(const float& l);

	~Hexagon();

	float Perimeter() const override;
};

