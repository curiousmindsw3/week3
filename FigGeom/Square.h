#pragma once
#include "GeometricShape.h"
class Square : public GeometricShape
{
protected: 
	float m_length;
public:
	float Perimeter()const  override;

	Square();

	Square(const float& l);

	float getLength() const;

	~Square();
};

