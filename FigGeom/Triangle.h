#pragma once
#include "ITriangle.h"
class Triangle : public ITriangle
{
protected:
	float m_length2;
public:
	float Perimeter()const  override;
	Triangle();
	~Triangle();
	Triangle(const float& l1, const float & l2, const float& l3);
};

