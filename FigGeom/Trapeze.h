#pragma once
#include "ITrapeze.h"

class Trapeze : public ITrapeze
{
protected:
	float  m_length3;

public:
	Trapeze();

	Trapeze(const float& l1, const float& l2, const float& l3, const float& l4);

	~Trapeze();

	float Perimeter() const override;
};

