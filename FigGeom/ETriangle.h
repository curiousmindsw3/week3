#pragma once
#include "GeometricShape.h"
class ETriangle : public GeometricShape
{
protected: 
	float m_length;
public:
	float Perimeter()const  override;
	ETriangle();
	ETriangle(const float& l);
	~ETriangle();
};

