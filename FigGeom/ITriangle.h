#pragma once
#include "ETriangle.h"
class ITriangle :
	public ETriangle
{
protected:
	float m_length1;
public:
	ITriangle();
	ITriangle(const float& l1, const float& l2);
	~ITriangle();
	float Perimeter() const override;
};

