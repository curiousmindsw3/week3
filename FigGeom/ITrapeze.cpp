#include "ITrapeze.h"



ITrapeze::ITrapeze()
{
}

ITrapeze::ITrapeze(const float & l1, const float & l2, const float & l3):
	m_length(l1),
	m_length1(l2),
	m_length2(l3)
{
}

float ITrapeze::Perimeter() const
{
	return 2*m_length + m_length1 + m_length2;
}


ITrapeze::~ITrapeze()
{
}
