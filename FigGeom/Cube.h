#pragma once
#include "Square.h"

class Cube : public Square
{
public:
	Cube();

	Cube(const float& l);

	Cube(const Square& s);

	~Cube();

	float Perimeter() const override;
};

