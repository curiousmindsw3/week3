#include "ETriangle.h"

float ETriangle::Perimeter()const
{
	//std::cout << "Equilateral triangle perimeter is: " << m_length * 3 << std::endl;
	return m_length * 3;
}

ETriangle::ETriangle()
{
}

ETriangle::ETriangle(const float & l):
	m_length(l)
{

}

ETriangle::~ETriangle()
{
}
