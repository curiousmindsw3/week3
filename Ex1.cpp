#include <iostream>
#include <algorithm>
#include "Point.h"

using namespace std;

//double distance(double a, double b)
//{
//	return fabs(a - b);
//}
//
//bool Compare(const double& number1, const double& number2, const double& parameter)
//{
//	return abs((parameter - number1)) > abs((parameter - number2));
//}

//void sortAccordingToDistance(double* entriesToSort, int count, double origin)
//{
//	//sort(entriesToSort, entriesToSort + count, Compare);
//
//	/*for (int index = 0; index < count - 1; ++index)
//	{
//		for (int j = index + 1; j < count; ++j)
//		{
//			if (Compare(entriesToSort[index], entriesToSort[j], origin))
//				swap(entriesToSort[index], entriesToSort[j]);
//		}
//	}*/
//}

//int main()
//{
//	int size;
//
//	cout << "Enter size: ";
//	cin >> size;
//
//	double* entries = new double[size];
//
//	cout << "Enter elements: ";
//	for (int index = 0; index < size; ++index)
//	{
//		cin >> entries[index];
//	}
//
//		sortAccordingToDistance(entries, size, 0);
//
//		cout << "Sorted array: ";
//	for (int i = 0; i < size; i++)
//		std::cout << entries[i] << std::endl;
//
//		return 0;
//}




void sortAccordingToDistance(Point* entriesToSort, int count, Point origin)
{
	for (int i = 0; i < count - 1; ++i)
	{
		for (int j = i + 1; j < count; ++j)
		{
			if (distance(entriesToSort[i], origin) > distance(entriesToSort[j], origin))
			{
				swap(entriesToSort[i], entriesToSort[j]);
			}
		}
	}
}

int main()
{
	int size;

	cout << "size = ";
	cin >> size;

	Point* entries = new Point[size];
	double currentX, currentY;

	for (int index = 0; index < size; ++index)
	{
		cin >> currentX >> currentY;

		entries[index].SetX(currentX);
		entries[index].SetY(currentY);
	}

	sortAccordingToDistance(entries, size, { 0.0, 0.0 });

	cout << endl;

	for (int i = 0; i < size; i++)
	{
		std::cout << entries[i].GetX() << " " << entries[i].GetY() << " distance : " << distance(entries[i], { 0.0, 0.0 }) <<std::endl;

	}
	


		return 0;
}
