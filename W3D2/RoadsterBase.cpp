#include "RoadsterBase.h"

RoadsterBase::RoadsterBase(int seats) :
	BaseCar(4),
	m_seatsCount(seats)
{
	std::cout << "Constructed RoadsterBase." << std::endl;
}

RoadsterBase::~RoadsterBase()
{
}

int RoadsterBase::GetSeatsCount() const
{
	return m_seatsCount;
}
