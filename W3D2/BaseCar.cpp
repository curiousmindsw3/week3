#include "BaseCar.h"


BaseCar::BaseCar(int wheels) :
	m_wheelsCount(wheels)
{
	std::cout << "Constructed BaseCar." << std::endl;
}

BaseCar::~BaseCar()
{
}

int BaseCar::GetWheelsCount() const
{
	return m_wheelsCount;
}
