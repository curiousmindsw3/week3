#pragma once
#include "BaseCar.h"

class RoadsterBase :public BaseCar
{
public:
	RoadsterBase( int seats);

	~RoadsterBase();

	int GetSeatsCount() const;

private:
	int m_seatsCount;
};

