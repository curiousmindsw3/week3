#pragma once
#include <iostream>

class BaseCar
{
public:
	BaseCar(int wheels);

	~BaseCar();

	int GetWheelsCount() const;

private:
	int m_wheelsCount;
};


