#include "Roadster.h"

Roadster::Roadster():
	RoadsterBase(3)
{
	std::cout << "Constructed Roadster." << std::endl;
}

Roadster::~Roadster()
{
}

Rgb Roadster::GetColor()
{
	return m_color;
}
