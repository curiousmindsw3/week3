#pragma once
#include "RoadsterBase.h"
#include "Rgb.h"

class Roadster : public RoadsterBase
{
public:
	Roadster();

	~Roadster();

	Rgb GetColor();

private:
	Rgb m_color;
};

